package com.hiep.example.client.activities.home;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.hiep.example.client.activities.basic.BasicWebViewImpl;

public class HomeViewImpl extends BasicWebViewImpl implements HomeView {
	
	private HomeViewImplUiBinder uiBinder = GWT.create(HomeViewImplUiBinder.class);
	interface HomeViewImplUiBinder extends UiBinder<Widget, HomeViewImpl> {
	}
	
	public HomeViewImpl() {	 
		super();
		this.getMainPanel().add(uiBinder.createAndBindUi(this));
	}
}
