package com.hiep.example.client.activities.home;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hiep.example.client.activities.ClientFactory;
import com.hiep.example.client.activities.basic.BasicWebActivity;

public class HomeActivity extends BasicWebActivity {
	
	private HomeView view;
	
	public HomeActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getHomeView();
		super.start(panel, eventBus, view);
		panel.setWidget(view);
	}
}
