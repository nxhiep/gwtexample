package com.hiep.example.client.activities.basic;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.hiep.example.client.activities.ClientFactory;

public class BasicWebActivity extends AbstractActivity {
	
	protected final ClientFactory clientFactory;
	protected EventBus eventBus;
	protected Place place = null;
	protected BasicWebView basicView = null;
	protected List<HandlerRegistration> handlerRegistrations = new ArrayList<HandlerRegistration>();
	protected List<com.google.gwt.event.shared.HandlerRegistration> oldHandlers = new ArrayList<com.google.gwt.event.shared.HandlerRegistration>();
	
	
	public BasicWebActivity(ClientFactory clientFactory, Place place) {
		this.clientFactory = clientFactory;
		this.place = place;
	}
	
	protected void addHandlerRegistration(HandlerRegistration registration) {
		handlerRegistrations.add(registration);
	}
	
	protected void addHandlerRegistration(com.google.gwt.event.shared.HandlerRegistration handlerRegistration) {
	    oldHandlers.add(handlerRegistration);
	}	
	protected void removeHandlerRegistration() {
		for(HandlerRegistration registration : handlerRegistrations) {
			registration.removeHandler();
		}
		handlerRegistrations.clear();
		
		for(com.google.gwt.event.shared.HandlerRegistration registration : oldHandlers) {
			registration.removeHandler();
		}
		oldHandlers.clear();
	}
	
	@Override
	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		this.eventBus = eventBus;
	}
	
	public void start(AcceptsOneWidget panel, final EventBus eventBus, final BasicWebView basicWebView) { 
		this.eventBus = eventBus;
		bind();
		this.basicView = basicWebView;
		if(basicView != null) {
			basicView.refreshView();
		}
		loadData();
	}
	
	protected void loadData() {
		
	}
	
	protected void bind() {
		removeHandlerRegistration();
		addHandlerRegistration(Window.addResizeHandler(new ResizeHandler() {
			
			@Override
			public void onResize(ResizeEvent event) {
				
			}
		}));
	}
	
	
	protected void goTo(Place newPlace) {
		
		clientFactory.getPlaceController().goTo(newPlace);
	}
	
	@Override
	public void onStop() {
		super.onStop();
		removeHandlerRegistration();
	}
}
