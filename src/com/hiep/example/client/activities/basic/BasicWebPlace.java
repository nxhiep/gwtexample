package com.hiep.example.client.activities.basic;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class BasicWebPlace extends Place {
	protected String token = "";
	protected Place previousPlace = null;
	
	 
	public BasicWebPlace() {
	}
	 
	public BasicWebPlace(Place previousPlace){
		this.previousPlace = previousPlace;
	}
	
	public BasicWebPlace(String token) {
		this.token = token;
	}
	
	public void setPreviousPlace(Place place) {
		this.previousPlace = place;
	}
	
	public Place getPreviousPlace() {
		if(previousPlace != null) {
			return previousPlace;
		}
		else return null;
	}

	public String getToken() {
		return this.token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	
	@Prefix("")
	public static class Tokenizer implements PlaceTokenizer<BasicWebPlace> {
		@Override
        public String getToken(BasicWebPlace place) {
            return  place.getToken();
        }

        @Override
        public BasicWebPlace getPlace(String token) {
            return new BasicWebPlace();
        }
    }
	
//	public static String getHash(Object object) {
//		String hash = Window.Location.getHash();
//		if(hash.isEmpty()) {
//			hash = Token.HOME_PAGE;
//		}
//		String more = "";
//		if(object == null)
//			return hash;
//		if(object instanceof Topic) {
//			more = ClientUtils.removeVietnameseChar(((Topic)object).getName());
//		}
//		if(!more.isEmpty()) {
//			more = more.replace(" ", "_");
//			hash += "/" + more;
//		}
//		return hash;
//	}
}
