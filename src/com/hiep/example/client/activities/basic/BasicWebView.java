package com.hiep.example.client.activities.basic;

import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.hiep.example.client.activities.basic.BasicWebViewImpl.Layout;

public interface BasicWebView extends IsWidget{
	void refreshView();

	Layout getLayout();
	
	HTMLPanel getMainPanel();
}
