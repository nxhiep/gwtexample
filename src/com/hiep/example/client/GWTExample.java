package com.hiep.example.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.hiep.example.client.activities.AppPlaceHistoryMapper;
import com.hiep.example.client.activities.AsyncActivityManager;
import com.hiep.example.client.activities.AsyncActivityMapper;
import com.hiep.example.client.activities.ClientFactory;
import com.hiep.example.client.activities.ClientFactoryImpl;
import com.hiep.example.client.activities.SplitAppActivityMapper;
import com.hiep.example.client.activities.home.HomePlace;

public class GWTExample implements EntryPoint {
	
	public static ClientFactory clientFactory = new ClientFactoryImpl();
	private static AppPlaceHistoryMapper historyMapper;
	
	public void onModuleLoad() {
		SimplePanel display = new SimplePanel();
		AsyncActivityMapper activityMapper = new SplitAppActivityMapper(clientFactory);
		AsyncActivityManager activityManager = new AsyncActivityManager(activityMapper, clientFactory.getEventBus());
		activityManager.setDisplay(display);
		final PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
		historyHandler.register(clientFactory.getPlaceController() , clientFactory.getEventBus(), new HomePlace());
		historyHandler.handleCurrentHistory();
		RootPanel.get("content").add(display);
	}
	
	public static AppPlaceHistoryMapper getHistoryMapper() {
		return historyMapper;
	}
}
